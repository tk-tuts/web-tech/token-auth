/**
 * Created by Keerthikan on 26-Feb-17.
 */
const express = require('express'),
    userController = require('../controller/user-controller');

const router = express.Router();


/** POST /api/auth/token Get JWT authentication token */
 router.route('/token')
    .post(userController.authenticate,
        userController.generateToken,
        userController.respondJWT);

module.exports = router;